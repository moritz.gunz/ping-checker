FROM alpine:latest

RUN apk add --no-cache iputils

WORKDIR /opt/pings
COPY main.sh .
RUN chmod +x main.sh

WORKDIR /pings
RUN mkdir -p /pings
ENTRYPOINT ["/opt/pings/main.sh"]
