#!/bin/sh

echo "Running ping loop. Press Ctrl+C to stop."

while true
do
    ping -c 1 1.1.1.1 > /dev/null || echo "$(date) - 1.1.1.1 unreachable" 1>&2
    sleep 10
done
