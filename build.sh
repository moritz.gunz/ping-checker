#!/bin/sh
set -e

docker buildx rm build-ping-checker > /dev/null || true
docker buildx create --name build-ping-checker --use
docker buildx inspect --bootstrap
docker buildx build --platform linux/arm64,linux/amd64,linux/arm/v7,linux/arm/v6 -t registry.gitlab.com/moritz.gunz/ping-checker . --push
docker buildx rm build-ping-checker
